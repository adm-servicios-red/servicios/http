FROM nginx:latest

SHELL ["/bin/bash", "-c"]

ENV DOMAIN localhost
ENV SUBJECT "/C=MX/ST=CDMX/L=CDMX/O=ESCOM/OU=ASR/CN=${DOMAIN}"

RUN apt update
RUN apt install openssl

RUN mkdir /etc/nginx/ssl
RUN openssl req -x509 -nodes -days 365 \
    -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key \
    -out /etc/nginx/ssl/nginx.crt \
    -subj ${SUBJECT}

#COPY nginx.conf /etc/nginx/conf.d/server.conf

RUN mkdir -p /var/site
#VOLUME ./site:/var/site
#VOLUME ./nginx.conf:/etc/nginx/conf.d/server.conf

#COPY index.html /var/site/index.html
#COPY style.css /var/site/style.css

RUN service nginx restart

