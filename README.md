
# if domain is different than localhost. e.g. lvnar.com
$ sed -i "s|localhost|localhost lvnar.com|" /etc/hosts

# build image and run container
$ docker build -t http .
$ docker run -p 80:80 -p 443:433 -e DOMAIN=lvnar.com -d -v "$PWD/site:/var/site" -v "$PWD/nginx.conf:/etc/nginx/conf.d/server.conf" --name http http 